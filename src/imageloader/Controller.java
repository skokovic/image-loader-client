package imageloader;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.IOException;


public class Controller {

    @FXML
    private TextField textFieldIP;

    @FXML
    private TextField textFieldPort;

    @FXML
    private ComboBox comboImageType;

    @FXML
    private Label labelIP;

    @FXML
    private Label labelPort;

    @FXML
    private Label labelImageType;

    @FXML
    private Label labelError;

    @FXML
    private Button submitButton;

    @FXML
    private ImageView imageView1;


    @FXML
    private void fetchImage(ActionEvent event) {

        String imageType = "1";
        if (comboImageType.getValue() != null) {
            String comboValue = comboImageType.getValue().toString();
            if (comboValue.equals("Normal")) {
                imageType = "1";
            } else if (comboValue.equals("Test")) {
                imageType = "0";
            } else if (comboValue.equals("Shifted Test")) {
                imageType = "2";
            }
        }

        try {
            Image image = SwingFXUtils.toFXImage(Client.get_image(textFieldIP.getText(), Integer.parseInt(textFieldPort.getText()), imageType), null);
            imageView1.setImage(image);
        } catch (IOException ex) {
            System.out.println("IOException caught => " + ex.toString());
            File file = new File("/Users/tom/Fucks/diplpro/src/imageloader/bars.gif");
            Image image = new Image(file.toURI().toString());
            imageView1.setImage(image);
            labelError.setVisible(true);
        }
    }


}
