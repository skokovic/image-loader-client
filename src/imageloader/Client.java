package imageloader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	
	public static BufferedImage get_image(String ip, int port, String tip_slike) throws UnknownHostException, IOException {

	BufferedWriter outputStream = null;
	BufferedReader inputStream = null;
	DataOutputStream outputToZynq;
	FileOutputStream outputToFile;
	File imageOutputFile;
	InputStream inputFromZynq;
	BufferedImage fetchedImage;

//	private static final String ip = "192.168.3.10";
//  private static final int port = 7777;
    
    Socket connectionToZynq = null;
    
    connectionToZynq = new Socket(ip, port);
	
	OutputStream outputToServer = connectionToZynq.getOutputStream();
	InputStream inputFromServer = connectionToZynq.getInputStream();
	DataInputStream in = new DataInputStream(connectionToZynq.getInputStream());

	byte imgType = 0x00;

	/*	ako je obična slika*/
	if (tip_slike.equals("0")) { imgType = 0x00; }

	/*	ako je testbar*/
	else if (tip_slike.equals("1")) { imgType = 0x01; }

	/*	ako je pomaknuti testbar*/
	else { imgType = 0x02; }

	long totalBytes = 0;
	connectionToZynq = new Socket(ip, port);
	outputToZynq = new DataOutputStream(connectionToZynq.getOutputStream());
	outputToZynq.writeByte(imgType);
    outputToZynq.flush();
    
    imageOutputFile = new File("original_image.jpg");
    outputToFile = new FileOutputStream(imageOutputFile);
    byte[] buffer = new byte[8 * 1024];
    int bytesRead;
    inputFromZynq = connectionToZynq.getInputStream();
    while ((bytesRead = inputFromZynq.read(buffer)) != -1) {
        totalBytes += bytesRead;
        for (int i = 0; i < bytesRead; i++) {
            buffer[i] = (byte) (buffer[i]);
        }
        outputToFile.write(buffer, 0, bytesRead);
    }
    
    
    fetchedImage = new BufferedImage(640, 480, BufferedImage.TYPE_INT_ARGB);
    fetchedImage = ImageIO.read(imageOutputFile);
    inputFromZynq.close();
    outputToFile.close();
    outputToZynq.close();
    connectionToZynq.close();
    
    return fetchedImage;

	}
}
